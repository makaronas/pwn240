#include "sll.h"
struct snode_t *
_make_snode(void)
{
	struct snode_t * n;
	n = (struct snode_t *)malloc(sizeof(struct snode_t));
	if (n == NULL){
		errno = ENOMEM;
		return NULL;
	}
	n->data = NULL;
	n->next = NULL;
	return n;
}
void
_destroy_snode(struct snode_t *node)
{
	if (node != NULL){
		node->next = NULL;
		free(node);
	}
}
struct sll_t *
make_sll(void)
{
	struct sll_t *new_list = (struct sll_t *)malloc(sizeof(struct sll_t));
	if (new_list == NULL){
		errno = ENOMEM;
		return NULL;
	}
	new_list->len = 0;
	new_list->head = NULL;
	new_list->prepend = &_prepend_sll;
	new_list->append = &_append_sll;
	new_list->insertAt = &_insertAt_sll;
	new_list->get = &_get_sll;
	new_list->remove = &_remove_sll;
	new_list->destroy = &_destroy_sll;
	return new_list;
}
void
_prepend_sll(struct sll_t *list, void *data)
{
	struct snode_t *temp;
	temp = NULL;
	if (list == NULL){
		errno = EINVAL;
		return;
	}
	if (list->len > 0){
		temp = list->head;
	}
	list->head = _make_snode();
	list->head->data = data;
	list->head->next = temp;
	list->len += 1;
}
void
_append_sll(struct sll_t *list, void *data)
{
	struct snode_t *iter;
	if (list == NULL){
		errno = EINVAL;
		return;
	}
	if (list->len == 0){
		_prepend_sll(list, data);
		return;
	}
	iter = list->head;
	while (iter->next!=NULL){
		iter = iter->next;
	}
	iter->next = _make_snode();
	iter->next->data = data;
	list->len += 1;
}
void
_insertAt_sll(struct sll_t *list, void *data, size_t index)
{
	size_t i;
	struct snode_t *iter, *new_node;
	if (list == NULL || index > list->len){
		errno = EINVAL;
		return;
	}
	i = 0;
	iter = list->head;
	if (index == 0){
		_prepend_sll(list, data);
		return;
	}
	while (i != index-1){
		iter = iter->next;
		++i;
	}
	new_node = _make_snode();
	new_node->data = data;
	new_node->next = iter->next;
	iter->next = new_node;
}
void *
_get_sll(struct sll_t *list, size_t index)
{
	size_t i;
	struct snode_t *iter; 
	if (list == NULL || index >= list->len ){
		errno = EINVAL;
		return NULL;
	}
	i = 0;
	iter = list->head;
	while (i != index){
		iter = iter->next;
		++i;
	}
	return iter->data;
}
void
_remove_sll(struct sll_t *list, size_t index)
{
	size_t i;
	struct snode_t *iter, *temp;
	if (list == NULL || index >= list->len ){
		errno = EINVAL;
		return;
	}
	i = 0;
	iter = list->head;
	if (index == 0){
		temp = list->head;
		list->head = temp->next;
		_destroy_snode(temp);
	}
	else {
		while (i != index-1){
			iter = iter->next;
			++i;
		}
		temp = iter->next->next;
		iter->next = temp;
		_destroy_snode(iter->next);
		
	}
	list->len -= 1;
}
void
_destroy_sll(struct sll_t *list)
{
	if (list == NULL){
		errno = EINVAL;
		return;
	}
	while (list->len > 0){
		_remove_sll(list, 0);
	}
}
