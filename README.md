# PWN240 #

Use the data structure implementations in c here to pass cs240.
The owner of this repo and the author of the code are not responsible
for any use of this code.

### How do I get set up? ###

* each data structure has their own make functions and contain function pointers to their methods
* each method's first argument is it's data structure
* each data structure that stores key/value data needs a (user supplied) function pointer to compare the keys
```
/*example code*/ 
#include "osll.h" /* include the ordered singly linked list */

struct key{
	char *s;
};
struct data{
	char *s;
	int num;
};
struct key *makeKey(char *oth){
	struct key* k = malloc(sizeof(struct key));
	k->s = strdup(oth);
	return k;
}
struct data *makeData(){
	struct data* d = malloc(sizeof(struct data));
	return d;
}
/* function to compare the key structs */
int cmp(void *k1, void *k2){
	return strcmp(((struct key *)k1)->s, ((struct key *)k2)->s);
}

int main(){

	struct ordered_sll_t *list;
	list = make_ordered_sll(&cmp); /* create the list */
	struct data *d = makeData();
	struct data *oth;
	d->num = 1;
	list->insert(list, makeKey("f"), d); /* insert example */
	oth = (struct data*)list->get(list, makeKey("f")); /* get example */
	list->remove(list, makeKey("f")); /* remove example */
	list->destroy(list); /* destroy example */
}

``` 

### Who do I talk to? ###

* makaron@csd.uoc.gr