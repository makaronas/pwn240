#ifndef __BST_H__
#define __BST_H__

#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

struct bstnode_t {
	void *key;
	void *data;
	struct bstnode_t *left, *right;
};

struct bstnode_t * _make_bstnode(void);
void _destroy_bstnode(struct bstnode_t * );

struct bst_t {
	struct bstnode_t *root;
	size_t size;
	int (*compare)(void *, void *);
	void (*insert)(struct bst_t *, void *, void * );
	void (*remove)(struct bst_t *, void * );
	void * (*get)(struct bst_t *, void * );
	void (*destroy)(struct bst_t * );

};
struct bst_t * make_bst(int (*)(void *, void *));
struct bstnode_t * _get_parent_bst(struct bst_t *, void * );
void _insert_bst(struct bst_t *, void *, void * );
void _remove_bst(struct bst_t *, void * );
void * _get_bst(struct bst_t *, void * );
void _destroy_bst(struct bst_t * );

#endif