#ifndef __SLL_H__
#define __SLL_H__

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

struct snode_t{
	void *data;
	struct snode_t *next;
};
struct snode_t* _make_snode(void);
void _destroy_snode(struct snode_t *);
struct sll_t{
	struct snode_t* head;
	size_t len;
	void (*prepend)(struct sll_t *, void *);
	void (*append)(struct sll_t *, void *);
	void (*insertAt)(struct sll_t *, void *, size_t );
	void * (*get)(struct sll_t *, size_t );
	void (*remove)(struct sll_t *, size_t );
	void (*destroy)(struct sll_t *);
};
struct sll_t * make_sll(void);
void _prepend_sll(struct sll_t *, void *);
void _append_sll(struct sll_t *, void *);
void _insertAt_sll(struct sll_t *, void *, size_t );
void * _get_sll(struct sll_t *, size_t );
void _remove_sll(struct sll_t *, size_t );
void _destroy_sll(struct sll_t *);

#endif
