#include "osll.h"

struct osnode_t*
_make_osnode(void)
{
	struct osnode_t *node = (struct osnode_t *)malloc(sizeof(struct osnode_t));
	if (node == NULL){
		errno = ENOMEM;
		return NULL;
	} 
	node->key = NULL;
	node->data = NULL;
	node->next = NULL;
	return node;
}
void
_destroy_osnode(struct osnode_t *node)
{
	if (node == NULL){
		errno = EINVAL;
		return;
	}
	free(node);
}
struct ordered_sll_t *
make_ordered_sll(int (*compare)(void *, void * ))
{
	if (compare == NULL){
		errno = EINVAL;
		return NULL;
	}
	struct ordered_sll_t *list = (struct ordered_sll_t *)malloc(sizeof(struct ordered_sll_t));
	if (list == NULL){
		errno = ENOMEM;
		return NULL;
	}
	list->head = NULL;
	list->len = 0;
	list->compare = compare;
	list->insert = &_insert_ordered_sll;
	list->get = &_get_ordered_sll;
	list->remove = &_remove_ordered_sll;
	list->destroy = &_destroy_ordered_sll;
	return list;
}

void 
_insert_ordered_sll(struct ordered_sll_t *list, void *key, void *data )
{
	if (
		list == NULL
		|| key == NULL
		){
		errno = EINVAL;
		return;
	}
	int c;
	struct osnode_t *prev, *node = _make_osnode();
	node->key = key;
	node->data = data;

	if (list->len == 0){
		list->head = node;
	}
	else {
		c = list->compare(list->head->key, node->key);	
		if (c == 0){
			_destroy_osnode(node);
			return;
		}
		else if (c < 0){
			for (prev = list->head; prev->next!=NULL;prev=prev->next){
				c = list->compare(prev->next->key, node->key);
				if (c == 0){
					_destroy_osnode(node);
					return;
				}
				else if (c > 0){
					break;
				}
			}
			if (prev->next != NULL){
				node->next = prev->next;
			}
			prev->next = node;
		}
		else {
			node->next = list->head;
			list->head = node;
		}
	}
	list->len += 1;
}
void *
_get_ordered_sll(struct ordered_sll_t *list, void *key )
{
	if (
		list == NULL
		|| key == NULL
		){
		errno = EINVAL;
		return NULL;
	}
	int c;
	struct osnode_t *iter;
	for (iter = list->head;iter!=NULL;iter = iter->next){
		c = list->compare(iter->key, key);
		if (c == 0){
			return iter->data;
		}
		else if (c > 0){
			break;
		}
	}
	return NULL;
}
void
_remove_ordered_sll(struct ordered_sll_t *list, void *key )
{
	if (
		list == NULL
		|| key == NULL
		){
		errno = EINVAL;
		return;
	}
	int c;
	struct osnode_t *prev, *tmp;
	if (list->len == 0){
		return;
	}
	c = list->compare(list->head->key, key);
	if (c == 0){
		tmp = list->head;
		list->head = list->head->next;
	}
	else if  (c < 0){
		return;
	}
	else{
		for (prev = list->head;prev->next!=NULL;prev = prev->next){
			c = list->compare(prev->next->key, key);
			if (c == 0){
				tmp = prev->next;
				if (prev->next->next != NULL){
					prev->next = prev->next->next;
				}
				else{
					prev->next = NULL;
				}
				break;
			}
			else if (c > 0){
				return;
			}
		}
	}
	_destroy_osnode(tmp);
	list->len -= 1;
}
void
_destroy_ordered_sll(struct ordered_sll_t *list)
{
	if (
		list == NULL
		){
		errno = EINVAL;
		return;
	}
	struct osnode_t *tmp;
	int i=0;
	while (list->len > 0){
		_remove_ordered_sll(list, list->head->key);
	}
	free(list);
}