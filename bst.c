#include "bst.h"

struct bstnode_t *
_make_bstnode(void)
{
	struct bstnode_t *node;
	node = (struct bstnode_t *)malloc(sizeof(struct bstnode_t ));
	if (node == NULL){
		errno = ENOMEM;
		return NULL;
	}
	node->data = NULL;
	node->key = NULL;
	node->left = NULL;
	node->right = NULL;
	return node;
}
void
_destroy_bstnode(struct bstnode_t *node )
{
	if (node == NULL){
		errno = EINVAL;
		return;
	}
	node->data = NULL;
	node->key = NULL;
	node->left = NULL;
	node->right = NULL;
	free(node);
}
struct bst_t *
make_bst(int (*cmp)(void *key1, void *key2) )
{
	struct bst_t *tree;
	if (cmp == NULL){
		errno = EINVAL;
		return NULL;
	}
	tree = (struct bst_t *)malloc(sizeof(struct bst_t));
	if (tree == NULL){
		errno = ENOMEM;
		return NULL;
	}
	tree->root = 0;
	tree->size = 0;
	tree->compare = cmp;
	tree->insert = &_insert_bst;
	tree->remove = &_remove_bst;
	tree->get = &_get_bst;
	tree->destroy = _destroy_bst;
	return tree;
}

struct bstnode_t *
_get_parent_bst(struct bst_t *tree, void *key )
{
	struct bstnode_t *iter, *prev;
	if (tree == NULL || key == NULL){
		errno = EINVAL;
		return NULL;
	}
	iter = tree->root;
	if (tree->compare((void  *)iter->key, key) == 0){
		return NULL;
	}
	else if (tree->compare((void  *)iter->key, key) < 0){
		iter = iter->right;
	}
	else {
		iter = iter->left;
	}
	prev = tree->root;
	while (iter != NULL){
		if (tree->compare((void  *)iter->key, key) == 0){
			return prev;
		}
		else if (tree->compare((void  *)iter->key, key) < 0){
			prev = iter;
			iter = iter->right;
		}
		else {
			prev = iter;
			iter = iter->left;
		}	
	}
	return NULL;
}
void
_insert_bst(struct bst_t *tree, void *key, void *data )
{
	struct bstnode_t *new_node, *iter = NULL;
	if (tree == NULL || key == NULL ){
		errno = EINVAL;
		return;
	}
	new_node = _make_bstnode();
	new_node->key = key;
	new_node->data = data;
	if (tree->root == NULL){
		tree->root = new_node;
		tree->size += 1;
	}
	else {
		iter = tree->root;
		while (iter!=NULL){
			if (tree->compare(iter->key, key) == 0){
				return;
			}
			else if (tree->compare(iter->key, key) < 0){
				if (iter->right == NULL){
					iter->right = new_node;
					break;
				}
				iter = iter->right;
			}
			else {
				if (iter->left == NULL){
					iter->left = new_node;
					break;
				}
				iter = iter->left;
			}		
		}
		if (iter != NULL){
			tree->size += 1;
		}
	}
}
void
_remove_bst(struct bst_t *tree, void *key )
{
	struct bstnode_t *iter, *p, *temp, *temp2;
	int is_left;
	if (tree == NULL || key == NULL){
		errno = EINVAL;
		return;
	}
	if (tree->size == 0){
		return;
	}
	/* if node to be removed is the root */
	if (tree->compare(tree->root->key, key) == 0){
		temp = tree->root;
		if (temp->left == NULL 
			&& temp->right == NULL
			){
			tree->root = NULL;
			_destroy_bstnode(temp);
		}
		else if (temp->right == NULL) {
			tree->root = temp->left;
			_destroy_bstnode(temp);	
		}
		else if (temp->left == NULL) {
			tree->root = temp->right;
			_destroy_bstnode(temp);	
		}
		else {
			iter = temp->left;
			while (iter->right != NULL){
				iter = iter->right;
			}
			tree->root->key = iter->key;
			tree->root->data = iter->data;
			iter->key = key;
			return _remove_bst(tree, temp->key);		
		}
	}
	else {
		p = _get_parent_bst(tree, key);
		if (p == NULL) {
			return;
		}
		/* if node to be deleted is left child of parent */
		if (p->left != NULL 
			&& tree->compare(p->left->key, key) == 0
			){
			temp = p->left;
			is_left = 1;
		}
		else {
			temp = p->right;
			is_left = 0;
		}
		if (temp->left == NULL
			&& temp->right == NULL
			){
			if (is_left) {
				p->left = NULL;
			} 
			else {
				p->right = NULL;
			}
			_destroy_bstnode(temp);	  
		}
		else if (temp->right == NULL){	
			if (is_left) {
				p->left = temp->left;
			} 
			else {
				p->right = temp->left;
			}
			_destroy_bstnode(temp);	
		}
		else if (temp->left == NULL){
			if (is_left) {
				p->left = temp->right;
			} 
			else {
				p->right = temp->right;
			}
			_destroy_bstnode(temp);
		}
		else {
			iter = temp->left;
			while (iter->right != NULL){
				iter = iter->right;
			}
			temp->key = iter->key;
			temp->data = iter->data;
			iter->key = key;
			return _remove_bst(tree, key);	
		}
	}
	tree->size -= 1;
}
void *
_get_bst(struct bst_t *tree, void *key )
{
	struct bstnode_t *iter;
	if (tree == NULL || key == NULL){
		errno = EINVAL;
		return NULL;
	}	
	iter = tree->root;
	while (iter!=NULL){
		if (tree->compare(iter->key, key) == 0){
			return iter->data;
		}
		else if (tree->compare(iter->key, key) < 0){
			iter = iter->right;
		}
		else {
			iter = iter->left;
		}
	}
	return NULL;
}
void
_destroy_bst(struct bst_t *tree )
{
	if (tree == NULL){
		errno = EINVAL;
		return;
	}
	while (tree->size > 0){
		_remove_bst(tree, tree->root->key);
	}
}
