#ifndef __OSLL_H__
#define __OSLL_H__

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

struct osnode_t{
	void *data;
	void *key;
	struct osnode_t *next;
};
struct osnode_t* _make_osnode(void);
void _destroy_osnode(struct osnode_t *);
struct ordered_sll_t{
	struct osnode_t* head;
	size_t len;
	int (*compare)(void *, void * );
	void (*insert)(struct ordered_sll_t *, void *, void * );
	void * (*get)(struct ordered_sll_t *, void * );
	void (*remove)(struct ordered_sll_t *, void * );
	void (*destroy)(struct ordered_sll_t *);
};
struct ordered_sll_t * make_ordered_sll(int (*)(void *, void * ));
void _insert_ordered_sll(struct ordered_sll_t *, void *, void * );
void * _get_ordered_sll(struct ordered_sll_t *, void * );
void _remove_ordered_sll(struct ordered_sll_t *, void * );
void _destroy_ordered_sll(struct ordered_sll_t *);

#endif
